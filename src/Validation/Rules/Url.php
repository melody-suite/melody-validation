<?php

namespace Orchestra\Validation\Rules;

use Orchestra\Validation\Rule;

class Email extends Rule
{
   public function validate($value): bool
   {
      return filter_var($value, FILTER_VALIDATE_URL);
   }

   public function message($value, $key): string
   {
      return "$key is not a valid url.";
   }
}
