<?php

namespace Orchestra\Validation\Rules;

use Orchestra\Validation\Rule;

class Required extends Rule
{
   public function validate($value): bool
   {
      return !empty($value) || $value === 0 || $value === false;
   }

   public function message($value, $key): string
   {
      return "$key is required.";
   }
}
