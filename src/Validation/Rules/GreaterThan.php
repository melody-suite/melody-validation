<?php

namespace Orchestra\Validation\Rules;

use Orchestra\Validation\Rule;

class GreaterThan extends Rule
{
   public function validate($value): bool
   {
      return $value > $this->options["value"];
   }

   public function message($value, $key): string
   {
      return "$key must be greater than " . $this->options["value"] . ".";
   }
}
