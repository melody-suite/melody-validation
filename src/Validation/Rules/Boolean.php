<?php

namespace Orchestra\Validation\Rules;

use Orchestra\Validation\Rule;

class Email extends Rule
{
   public function validate($value): bool
   {
      return is_bool($value);
   }

   public function message($value, $key): string
   {
      return "$key is not a valid boolean.";
   }
}
