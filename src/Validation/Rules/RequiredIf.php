<?php

namespace Orchestra\Validation\Rules;

use Orchestra\Validation\Rule;

class RequiredIf extends Rule
{
   public function validate($value): bool
   {
      return !empty($this->data[$this->options['key']]) ? !empty($value) || $value === 0 || $value === false : true;
   }

   public function message($value, $key): string
   {
      return "$key is required.";
   }
}
