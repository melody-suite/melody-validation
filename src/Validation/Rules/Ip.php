<?php

namespace Orchestra\Validation\Rules;

use Orchestra\Validation\Rule;

class Ip extends Rule
{
   public function validate($value): bool
   {
      return filter_var($value, FILTER_VALIDATE_IP);
   }

   public function message($value, $key): string
   {
      return "$key is not a valid IP.";
   }
}
