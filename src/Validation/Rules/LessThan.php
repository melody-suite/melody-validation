<?php

namespace Orchestra\Validation\Rules;

use Orchestra\Validation\Rule;

class LessThan extends Rule
{
   public function validate($value): bool
   {
      return $value < $this->options["value"];
   }

   public function message($value, $key): string
   {
      return "$key must be less than " . $this->options["value"] . ".";
   }
}
