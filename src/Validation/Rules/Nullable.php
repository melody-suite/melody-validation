<?php

namespace Orchestra\Validation\Rules;

use Orchestra\Validation\Rule;

class Nullable extends Rule
{
   public function validate($value): bool
   {
      return true;
   }

   public function message($value, $key): string
   {
      return "";
   }

   public function gotToNextRule()
   {
      return !empty($this->data[$this->key]);
   }
}
