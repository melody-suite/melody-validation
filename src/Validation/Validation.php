<?php

namespace Orchestra\Validation;

use Orchestra\Exceptions\InvalidRuleException;
use Orchestra\Exceptions\InvalidValidationException;
use Orchestra\Exceptions\ValidationException;

class Validation
{
   protected $errors = [];

   protected $validated = [];

   public function run(array $validations, array $data)
   {
      if (empty($validations)) {
         throw new InvalidValidationException("Validations array is empty cannot be empty");
      }

      foreach ($validations as $key => $rules) {
         if (empty($rules)) {
            throw new InvalidRuleException("Rules to validate cannot be empty");
         }

         $value = empty($data[$key]) ? null : $data[$key];

         $valid = true;

         foreach ($rules as $rule) {

            if (is_string($rule)) {

               $rule = self::buildRuleFromString($rule);
            }

            if (!($rule instanceof Rule)) {
               throw new InvalidRuleException("$rule not instance of " . Rule::class);
            }

            $rule->setData($data);

            $rule->setKey($key);

            if (!$rule->gotToNextRule()) {
               break;
            }

            if (!$rule->validate($value)) {
               $this->errors[$key] = $rule->message($value, $key);

               $valid = false;
            }
         }

         if ($valid) {
            $this->validated[$key] = $value;
         }
      }

      if (!$this->isValid()) {
         throw new ValidationException("Invalid data", 422);
      }

      return $this;
   }

   public function getErrors(): array
   {
      return $this->errors;
   }

   public function getValidated(): array
   {
      return $this->validated;
   }

   public function isValid(): bool
   {
      return empty($this->errors);
   }

   private static function buildRuleFromString($rule)
   {
      $validationArray = explode("|", $rule);

      $rule = array_shift($validationArray);

      $rule = str_replace("_", '', ucwords($rule, "_"));


      $rule = "Orchestra\Validation\Rules\\" . ucfirst($rule);

      $options = [];

      array_walk($validationArray, function ($item) use (&$options) {

         $option = explode(",", $item);

         $options[$option[0]] = $option[1];
      });

      return new $rule($options);
   }
}
