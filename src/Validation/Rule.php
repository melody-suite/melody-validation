<?php

namespace Orchestra\Validation;

abstract class Rule
{
   protected $options;

   protected $key;

   protected $data;

   public function __construct($options = [])
   {
      $this->options = $options;

      $this->data = [];

      $this->key = null;
   }

   public abstract function validate($value): bool;

   public abstract function message($value, $key): string;

   public function setData(array $data)
   {
      $this->data = $data;
   }

   public function setKey(string $key)
   {
      $this->key = $key;
   }

   public function gotToNextRule()
   {
      return true;
   }
}
